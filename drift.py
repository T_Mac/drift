import config
import logging
import signal
from threading import Event
import sys
moduleLogger = config.getLogger('drift')

from lib.locker import TakeMasterLock
from lib.discover import Searcher
from lib.convert import Converter
import time

import uuid
WORKER_ID = config.CONFIG.get('WORKER_ID', uuid.uuid4().hex)

EXIT = Event()

def HandleShutdown():
	EXIT.set()

signal.signal(signal.SIGTERM, HandleShutdown) # Setup signal handling

MASTER_LOCK = TakeMasterLock()

searcher = None

converter = Converter(WORKER_ID)
converter.start()

while not EXIT.isSet():
	MASTER_LOCK.acquire(lock_ttl = 60, blocking = False)
	if MASTER_LOCK.is_acquired:
		if not searcher:
			moduleLogger.debug('MASTER_LOCK acquired, starting searcher')
			searcher = Searcher(config.INPUT_DIR)
			searcher.start()
		else:
			moduleLogger.debug('MASTER_LOCK acquired, searcher already running')


	time.sleep(30)

if MASTER_LOCK:
	searcher.join()
	converter.join()
