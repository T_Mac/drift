import logging
#import fittings
import collections
import config
moduleLogger = config.getLogger(__name__)

class Pipeline(object):
	def __init__(self):
		self.filters = collections.OrderedDict()

	def Register(self, filter, *args):
		self.filters[filter] = args
		return self

	def Execute(self, message):
		for filter, args in self.filters.items():
			message = filter(*args).Execute(message)
		return message

class Filter(object):
	def __init__(self):
		self.logger = moduleLogger.getChild(self.__class__.__name__)
	def Execute(self, msg):
		return msg


class Message(object):
	def __init__(self, **kwargs):
		self.StopProcessing = False
		self.Error = None
		self.Retry = False
		for key, value in kwargs.items():
			setattr(self, key, value)

	def __repr__(self):
		return 'SP: %s - Retry: %s - Error: %s - Data: %s'%(self.StopProcessing, self.Retry, self.Error, str(self.__dict__))
