from pipewrench.fittings import PipeFitting
import config
moduleLogger = config.getLogger('router')
class Router(object):
	def __init__(self, pipeline = None):
		self.logger = moduleLogger.getChild(self.__class__.__name__)
		if not pipeline:
			self.pipeline = PipeFitting()
		else:
			self.pipeline = pipeline

		self.Extend = self.pipeline.Register
		self.Route = self.pipeline.Invoke
		self.Setup()

	def Execute(self, msg):
		return self.Route(msg)

	def Setup(self):
		pass
