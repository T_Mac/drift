#Etcd locking and coordination goes here
import etcd
from etcd import EtcdCompareFailed
# import lib.job
import config
from lib.utils import dumpToEtcd, loadFromEtcd, CreateEtcdClient
import logging
from threading import Event, Thread
import time
from pipewrench.screens import Screen
from pipewrench.fittings import PipeFitting
import json
moduleLogger = config.getLogger('locker')

etcdClient = CreateEtcdClient()

def TakeLock(path):
	sanPath = utils.stripAllSpecial(path)
	lock = etcd.Lock(etcdClient, config.ETCD_ROOT[1:] + sanPath)
	moduleLogger.debug('Aquiring lock for %s'%path)
	lock.acquire(blocking=False, timeout=10)
	if lock.is_acquired:
		return lock

	return False


def TakeMasterLock():
	lockPath = config.ETCD_ROOT[1:] + '_master_lock'
	lock = etcd.Lock(etcdClient, lockPath)
	lock.acquire(blocking=False, timeout=10, lock_ttl=60)
	return lock

def updateJob(job, key, value):
	j = job.copy()
	j[key] = value
	return j

def createJsonPayload(job):
	return json.dumps(list(job.items()))

# def StartJob(job, jobKey, previousJobKey = None):
# 	moduleLogger.debug('Aquiring lock for %s'%job['path'])
# 	if previousJobKey:
# 		if not CheckJobStatus(previousJobKey, lib.job.FINISHED):
# 			moduleLogger.debug('Previous job not FINISHED for %s'%job['path'])
# 			return False
# 	newPayload = dumpToEtcd(updateJob(job, 'status', lib.job.STARTED))
# 	oldPayload = dumpToEtcd(job)
# 	return CompareAndSwap(jobKey, newPayload, oldPayload)
#
#
#
# def ReleaseJob(job, jobKey):
# 	moduleLogger.debug('Resetting lock for %s'%job['path'])
# 	newPayload = dumpToEtcd(job)
# 	oldPayload = dumpToEtcd(updateJob(job, 'status', lib.job.STARTED))
# 	return CompareAndSwap(jobKey, newPayload, oldPayload)
#
#
# def FinishJob(job, jobKey):
# 	moduleLogger.debug('Releasing lock for %s'%job['path'])
# 	newPayload = dumpToEtcd(updateJob(job, 'status', lib.job.FINISHED))
# 	oldPayload = dumpToEtcd(updateJob(job, 'status', lib.job.STARTED))
# 	return CompareAndSwap(jobKey, newPayload, oldPayload)

def CompareAndSwap(key, newValue, oldValue):
	try:
		etcdClient.write(key, newValue, prevValue=oldValue)
	except EtcdCompareFailed as e:
		moduleLogger.debug('Failed to CompareAndSwap %s new: %s old: %s'%(key, newValue, oldValue))
		moduleLogger.exception(e)
		return False
	else:
		return True

def CheckJobStatus(jobKey, status = None):
	results = etcdClient.read(jobKey)
	payload = loadFromEtcd(results.value)
	if status:
		return status == payload['status']
	return status

class UnlockScreen(Screen):
	def Execute(self, msg):
		msg = self.target(msg)
		if msg.StopProcessing:
			if config.CLUSTER_MODE:
				if getattr(msg, 'locked', False):
					self.logger.debug('Caught stop processing for %s releasing etcd lock'%msg.origin_path)
					if msg.Error:
						msg.MarkErrored(msg.Error)
					else:
						msg.ReleaseJob()
		return msg

class LockSafeFitting(PipeFitting):
	def convertFile(self, msg):
		unlock_screen = UnlockScreen(self.Invoke)
		return unlock_screen.Execute(msg)

class Relocker(Thread):
	def __init__(self, lock, interval=3000):
		self._lock = lock
		self.interval = interval
		self._exit = Event()
		super().__init__()
		self.daemon = True

	def run(self):
		while not self._exit.isSet():
			time.sleep(self.interval)
			self._lock.acquire()

	def join(self, *args, **kwargs):
		self._exit.set()
		super().join(*args, **kwargs)

	def acquire(self, *args, **kwargs):
		return self._lock.acquire(*args, **kwargs)

	def release(self):
		return self._lock.release()

# class JobLock():
#     def __init__(self, key_path):
#         self.keyPath = key_path
#
#     def _takeLock():
#
#     def _releaseLock():
#
#     def _setStatus():
#
#     def _setError():
#
#     def _setMachine():
#
