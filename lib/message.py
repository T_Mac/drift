import uuid
import etcd
import config
import json
from lib.pathOpt import findBaseName
import lib.job
from collections import OrderedDict, namedtuple
from lib.utils import loadFromEtcd, dumpToEtcd, CreateEtcdClient
import lib.locker
moduleLogger = config.getLogger('message')

etcdClient = CreateEtcdClient()

basePayload = OrderedDict([
	('path', None),
	('status', lib.job.QUEUED),
	('mode', None),
	('error', None),
	('worker', None)
])

EtcdBeforeAndAfter = namedtuple('EtcdBeforeAndAfter', ['before', 'current', 'after'])
SimpleEtcdResult = namedtuple('SimpleEtcdResult', ['key', 'value'])

def CreateConvertEntry(path):
	payload = buildPayload(path, lib.job.CONVERT)
	job = writeJobKey(path, payload)
	return job.key

def CreateConcatEntry(path):
	payload = buildPayload(path, lib.job.CONCAT)
	job = writeJobKey(path, payload)
	return job.key

def buildPayload(path, mode):
	payload = basePayload.copy()
	payload['path'] = path.as_posix()
	payload['mode'] = mode
	return payload

def writeJobKey(path, job):
	baseName = findBaseName(path)
	keyPath = config.ETCD_ROOT + '/%s'%baseName
	if job['mode'] == lib.job.CONCAT:
		return etcdClient.write(keyPath, dumpToEtcd(job), append=True)
	else:
		return etcdClient.write(keyPath, dumpToEtcd(job))

def iterEtcd(key):
	try:
		raw = etcdClient.read(key, recursive=True, sorted=True).children
	except:
		pass
	else:
		for entry in raw:
			yield lib.job.EtcdMessage(entry.key)

def iterEtcdWithBeforeAndAfter(key):
	raw = etcdClient.read(key, recursive=True, sorted=True).children
	results = [SimpleEtcdResult(x.key, loadFromEtcd(x.value)) for x in raw]
	for x in range(len(results)-1):
		if not x == 0:
			before = results[x-1]
		else:
			before = None

		current = results[x]

		if not x == len(results)-1:
			after = results[x+1]
		else:
			after = None
		yield EtcdBeforeAndAfter(before, current, after)


# def RemoveCompletedJobs():
# 	removed = []
# 	try:
# 		jobs = etcdClient.read(config.ETCD_ROOT).children
# 	except:
# 		pass
# 	else:
# 		for job in jobs:
# 			previous = None
# 			moduleLogger.debug('Inspecting %s for removal'%job.key)
# 			if not job.dir:
# 				payload = loadFromEtcd(job.value)
# 				if payload['status'] == lib.job.FINISHED and payload['mode'] == lib.job.CONVERT:
# 					etcdClient.delete(job.key)
# 					removed.append(payload['path'])
# 			else:
# 				moduleLogger.debug('Got directory, must be concat job %s'%job.key)
# 				for entry in iterEtcdWithBeforeAndAfter(job.key):
# 					if entry.current.value['status'] == lib.job.FINISHED and entry.after.value['status'] == lib.job.FINISHED:
# 						moduleLogger.debug('Removing job %s'%(entry.current.key))
# 						etcdClient.delete(entry.current.key)
# 						removed.append(entry.current.value['path'])
	# return removed
def RemoveCompletedJobs():
	needsRemoval = []
	for entry in iterEtcd(config.ETCD_ROOT):
		if entry.status == lib.job.Status.FINISHED:
			if entry.mode == lib.job.Type.CONCAT:
				nextKey = entry.nextKey
				prevKey = entry.prevKey
				canRemove = True
				if nextKey:
					if not nextKey.status == lib.job.Status.FINISHED:
						canRemove = False
				if prevKey:
					if not prevKey.status == lib.job.Status.FINISHED:
						canRemove = False
				if canRemove:
					needsRemoval.append(entry)
			else:
				needsRemoval.append(entry)

	moduleLogger.debug('Removing Finished Jobs %s'%str([(x.key, x.path) for x in needsRemoval]))
	for entry in needsRemoval:
		etcdClient.delete(entry.key)
	return [x.path for x in needsRemoval]


def GetJob():
	for entry in iterEtcd(config.ETCD_ROOT):
		if entry.mode == lib.job.Type.CONCAT:
			moduleLogger.debug('Got directory, must be concat job %s'%entry.key)
			for child in iterEtcd(entry.key):
				if child.status == lib.job.Status.QUEUED:
					prevKey = child.prevKey
					if prevKey:
						if prevKey.status == lib.job.Status.FINISHED:
							return child
					else:
						return child
		else:
			if entry.status == lib.job.Status.QUEUED:
				return entry

	return None

def getJobsFromEtcd():
	try:
		results = etcdClient.read(config.ETCD_ROOT, sorted = True).children
	except:
		results = []
	for entry in results:
		previous = None
		previousDetails = None
		if entry.dir:
			moduleLogger.debug('Got directory, must be concat job %s'%entry.key)
			children = etcdClient.read(entry.key, sorted=True).children
			for child in children:
				moduleLogger.debug('Child key for %s, %s - previous %s'%(entry.key, child.key, previous))
				if child.value:
					if previous: # If a previous job exists make sure its finished
						if not previousDetails['status'] == lib.job.FINISHED:
							continue
					details = loadFromEtcd(child.value)
					if details['status'] == lib.job.QUEUED:
						if lib.locker.StartJob(details, child.key, previous):
							return buildJobBlob(details, child.key, previous)


					previous = child.key
					previousDetails = details

		elif entry.value:
			details = loadFromEtcd(entry.value)
			if details['status'] == lib.job.QUEUED:
				if lib.locker.StartJob(details, entry.key, previous):
					return buildJobBlob(details, entry.key, previous)


def buildJobBlob(details, key, previous):
	payload = {
		'origin_path':details['path'],
		'rawJob':details,
		'mode':details['mode'],
		'jobKey':key,
		'previousJobKey':previous,
		'locked': True
		}
	return payload



def PreviousRun():
	try:
		etcdClient.read(config.ETCD_ROOT)
	except etcd.EtcdKeyNotFound:
		return False
	else:
		return True

def loadExistingJobs():
	try:
		jobs = etcdClient.read(config.ETCD_ROOT, recursive = True, sorted = True)

	except etcd.EtcdKeyNotFound:
		return []

	else:
		paths = []
		for job in jobs.children:
			if not job.dir:
				details = loadFromEtcd(job.value)
				paths.append(details['path'])
		return paths
