# File discovery goes here
import os
import pathlib
import config
import lib.job
from threading import Thread, Event
from lib.message import CreateConcatEntry, CreateConvertEntry, RemoveCompletedJobs, PreviousRun, loadExistingJobs
import time
moduleLogger = config.getLogger('discover')
file_extensions = [ 'avi', 'mkv', 'mp4', 'wmv', 'flv', 'm4v' ]

def searchForFiles(dir):
	rootPath = pathlib.Path(dir)
	for extension in file_extensions:
		for path in rootPath.rglob('*.%s'%extension):
			if filterMinimumSize(path):
				yield path

def filterMinimumSize(path):
	return path.stat().st_size >= config.MIN_FILE_SIZE

def ScanForJobs(dir):
	# unorderd = []
	convert = []
	concat = []
	for path in searchForFiles(dir):
		jobType = determineJobType(path)
		if jobType == lib.job.CONCAT:
			concat.append(path)
		elif jobType == lib.job.CONVERT:
			convert.append(path)
	return sorted(convert), sorted(concat)

def determineJobType(path):
	#Convert or Concat
	if '.part' in path.suffixes:
		return lib.job.CONCAT
	else:
		return lib.job.CONVERT

class Searcher(Thread):
	def __init__(self, filePath):
		self.searchPath = filePath
		self.currentJobs = []
		self._exit = Event()
		self.maxJobs = 50
		super().__init__()
		self.daemon = True

	def run(self):
		if PreviousRun():
			moduleLogger.debug('Previous run found')
			self.addExistingJobs()

		while not self._exit.isSet():
			self.removeCompleted()
			convert, concat = self.scanForJobs()
			self.addConvertJobs(convert)
			self.addConcatJobs(concat)
			time.sleep(int(config.POLL_TIME))

	def removeCompleted(self):
		removed = RemoveCompletedJobs()
		for entry in removed:
			self.currentJobs.remove(entry)

	def scanForJobs(self):
		return ScanForJobs(self.searchPath)

	def jobExists(self, path):
		return path in self.currentJobs

	def addConvertJobs(self, paths):
		for path in paths:
			if not self.jobExists(path) and len(self.currentJobs) <= self.maxJobs:
				moduleLogger.debug('Adding Convert Job %s'%path)
				CreateConvertEntry(path)
				self.currentJobs.append(path)

	def addConcatJobs(self, paths):
		for path in paths:
			if not self.jobExists(path) and len(self.currentJobs) <= self.maxJobs:
				moduleLogger.debug('Adding Concat Job %s'%path)
				CreateConcatEntry(path)
				self.currentJobs.append(path)

	def addExistingJobs(self):
		self.currentJobs = loadExistingJobs()
		moduleLogger.info('%i previous jobs added'%len(self.currentJobs))

	def join(self, *args, **kwargs):
		self._exit.set()
		return super().join(*args, **kwargs)
