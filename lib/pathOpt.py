import pathlib


def findBaseName(path):
	if not '.part' in path.suffixes:
		return path.stem

	else:
		stem = path.stem
		return stem[:stem.find('.part')]

def findPartNumber(path):
	stem = path.stem
	return stem[stem.find('.part')+6:].split('.')[0]
