from string import ascii_lowercase
ascii_lowercase = ascii_lowercase + '/' + '0123456789'
seperators = [' ',]

from collections import OrderedDict
import simplejson as json
import config
import etcd
moduleLogger = config.getLogger('utils')

def readFromConfig(key, path = 'config.json', default = None): 	# Reads a value from the config file in json format
	try:														# Default will be returned if the given key does not exist
		with open(path) as configFile:							#	or if there is ANY error reading the config file
			config = json.load(configFile)
		return config.get(key, default)
	except Exception as e:
		moduleLogger.error('Error reading %s from config file %s'%(key, path))
		moduleLogger.exception(e)

	return default

def copyFileToTemp(file_path):
	pass

def sanitizePathsForEtcd(file_path):
	step1 = file_path.replace(' ', '')
	step2 = step1.replace('-', '')
	step3 = step2.replace('_', '')
	return step3

def stripAllSpecial(text):
	text = str(text).lower()
	stripped = ''
	for letter in text:
		if letter in ascii_lowercase:
			stripped = stripped + letter
		elif letter in seperators:
			stripped = stripped + '_'
	return stripped

def loadFromEtcd(data):
	moduleLogger.debug('Got %s for loading'%data)
	parsed = json.loads(data, object_pairs_hook=OrderedDict)
	moduleLogger.debug('Parsed to %s'%parsed)
	return parsed

def dumpToEtcd(data):
	# pair = '("%s","%s")'
	# array = [pair%(x,y) for x, y in data.items()]
	# text = '['
	# for item in array:
	# 	text = text + item + ','
	# text = text[:-1] + ']'
	# moduleLogger.debug('Dumped dict to %s'%array)
	text = json.dumps(data)
	moduleLogger.debug('Dumped array to %s'%text)
	return text

def CreateEtcdClient():
	if config.CONFIG.get('ETCD_DNS', False):
		return etcd.Client(srv_domain=config.ETCD_DNS, allow_reconnect=True)
	else:
		return etcd.Client(host=config.ETCD_HOST, port=2379)

def findBaseName(path):
	if not '.part' in path.suffixes:
		return path.stem

	else:
		stem = path.stem
		return stem[:stem.find('.part')]
