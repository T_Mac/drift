from queue import Queue
from oset import oset as OrderedSet

class UniqueQueue(Queue):
	def _init(self, maxsize):
		self.queue = OrderedSet()
	def _put(self, item):
		self.queue.add(item)
	def _get(self):
		return self.queue.pop()
	def __contains__(self, item):
		with self.mutex:
			return item in self.queue
