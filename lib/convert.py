from threading import Thread, Event
import time
from lib.pool import WorkerPool
from lib.message import GetJob
import lib.job
import config
moduleLogger = config.getLogger('convert')

class Converter(Thread):
	def __init__(self, workerId):
		self.workerPool = WorkerPool(workerId)
		self._exit = Event()
		super().__init__()
		self.daemon = True

	def run(self):
		self.workerPool.startProcessing()
		while not self._exit.isSet():
			# for entry in getJobsFromEtcd():
			if not self.workerPool.isFull():
				job = GetJob()
				if job:
					moduleLogger.debug('Adding job %s'%job)
					self.workerPool.addJob(job)

				else:
					time.sleep(30)
			else:
				time.sleep(15)

		self.workerPool.stopProcessing()

	def waitForEmpty(self):
		while not self._exit.isSet():
			if self.workerPool.queueLength() == 0:
				break
			else:
				time.sleep(5)

	def join(self, *args, **kwargs):
		self._exit.set()
		self.workerPool.join(*args, **kwargs)
		return super().join(*args, **kwargs)
