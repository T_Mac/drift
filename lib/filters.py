from pipewrench.pipeline import Filter
from pipewrench.errors import StopProcessingError
from pipewrench.screens import Screen
from pipewrench.router import Router
import lib.locker
import tempfile
import shutil
from lib.ffmpeg import probeFile, convertFile, concatFile
import os
import config
import pathlib
from lib.pathOpt import findBaseName, findPartNumber
# filters in Order
# take lock
# create temp directory
# copy to disk
# discover file attrs
# determine encoding options
# convert file
# gen destination path
# copy to destination
# remove original file
# release lock

class TakeLock(Filter):
	def Execute(self, msg):
		self.logger.debug('Taking lock for %s'%msg.origin_path)
		if not msg.StartJob():
			raise StopProcessingError('Failed to take lock for %s'%msg.origin_path)
		else:
			return msg

class CreateTempDirectory(Filter):
	def Execute(self, msg):
		temp = tempfile.TemporaryDirectory()
		msg.tempDir = temp
		self.logger.debug('Created temp directory %s'%msg.tempDir.name)
		return msg

class CopyToDisk(Filter):
	def Execute(self, msg):
		self.logger.debug('Copying %s to %s'%(msg.origin_path, msg.tempDir.name))
		try:
			msg.tempInput = shutil.copy(str(msg.origin_path), msg.tempDir.name + '/input%s'%msg.origin_path.suffix)
		except Exception as e:
			msg.Error = str(e)
			raise e
		self.logger.debug(' Finished Copying %s to %s'%(msg.origin_path, msg.tempDir.name))
		return msg

class InspectFile(Filter):
	def Execute(self, msg):
		self.logger.debug('Inspecting %s for codec info'%msg.origin_path)
		info = probeFile(msg.tempInput)
		msg.originCodecVideo = info['video']
		msg.originCodecAudio = info['audio']
		self.logger.debug('Discovered vidoe: %s, audio: %s about %s'%(msg.originCodecVideo, msg.originCodecAudio, msg.origin_path))
		return msg

class SetupEncoding(Filter):
	def Execute(self, msg):
		msg.encodeVideo = True
		msg.encodeAudio = True

		if msg.originCodecVideo in config.ACCEPTABLE_CODECS:
			msg.encodeVideo = False



		if msg.originCodecAudio in config.ACCEPTABLE_CODECS:
			msg.encodeAudio = False
		self.logger.debug('Setup encoding - video: %s audio: %s for %s'%(msg.encodeVideo, msg.encodeAudio, msg.origin_path))
		return msg

class EncodeVideo(Filter):
	def Execute(self, msg):
		self.logger.debug('Coverting %s'%msg.origin_path)
		msg.tempOutput = msg.tempDir.name + '/output.' + config.FFMPEG_CONTAINER
		msg.return_code = convertFile(msg.tempInput, msg.tempOutput, msg.encodeVideo, msg.encodeAudio)
		self.logger.debug('Done Converting %s'%msg.origin_path)
		return msg

class CheckForOutput(Filter):
	def Execute(self, msg):
		self.logger.debug('Checking for encoded video...')
		if not os.path.isfile(msg.tempOutput):
			raise StopProcessingError('Encoded video not found for %s'%msg.origin_path)
		else:
			# Check to make sure the filesize isn't zero
			if not os.path.getsize(msg.tempOutput) > 0:
				raise StopProcessingError('Empty Encoded video for %s'%msg.origin_path)
		return msg

class DetermineDestination(Filter):
	def Execute(self, msg):
		base_path = msg.origin_path.relative_to(config.INPUT_DIR)
		if msg.mode == lib.job.CONVERT:
			msg.outputPath = pathlib.Path(config.OUTPUT_DIR).joinpath(base_path).with_suffix('.' + config.FFMPEG_CONTAINER)
		elif msg.mode == lib.job.CONCAT:
			dir_path = base_path.parent
			base_name = findBaseName(msg.origin_path)
			part_number = '_' + findPartNumber(msg.origin_path)
			msg.outputPath = pathlib.Path(config.OUTPUT_DIR).joinpath(dir_path, base_name+part_number).with_suffix('.' + config.FFMPEG_CONTAINER)

		# base_path = '/' + msg.origin_path.stem  + '.'
		# msg.outputPath = pathlib.Path(config.OUTPUT_DIR  + base_path + config.FFMPEG_CONTAINER)
		self.logger.debug('%s will be copied to %s'%(msg.origin_path, msg.outputPath))
		return msg




class CreateDestDirectories(Filter):
	def Execute(self, msg):
		self.logger.debug('Creating dest directories for %s'%msg.origin_path)
		create_path = os.path.dirname(str(msg.outputPath.as_posix()))
		self.logger.debug('Creating directory: %s'%create_path)
		try:
			os.makedirs(create_path, exist_ok=True)
		except FileExistsError as e:
			self.logger.debug('Creating path threw an error :(')
			self.logger.exception(e)

		return msg

class CopyToDestination(Filter):
	def Execute(self, msg):
		self.logger.debug('Copying %s to destination'%msg.origin_path)
		shutil.copyfile(msg.tempOutput, str(msg.outputPath))
		self.logger.debug('Done copying %s'%msg.origin_path)
		return msg

class CleanupTempDir(Filter):
	def Execute(self, msg):
		msg.tempDir.cleanup()
		self.logger.debug('Cleaned up temp directory %s'%msg.tempDir.name)
		return msg

class RemoveOriginal(Filter):
	def Execute(self, msg):
		self.logger.debug('Deleting original file - %s'%msg.origin_path)
		os.remove(str(msg.origin_path))
		return msg

class ReleaseLock(Filter):
	def Execute(self, msg):
		if config.CLUSTER_MODE:
			self.logger.debug('Releasing lock for %s'%msg.origin_path)
			# lib.locker.FinishJob(msg.rawJob, msg.jobKey)
			msg.FinishJob()
			return msg
		else:
			return msg

class DetermineConcatFile(Filter):
	def Execute(self, msg):
		msg.concatInput = msg.tempDir.name + '/' + msg.outputPath.name
		self.logger.debug('%s will be concatted with %s locally, %s remotely'%(msg.origin_path, msg.concatInput, msg.outputPath))
		return msg

class CopyConcatFile(Filter):
	def Execute(self, msg):
		if os.path.isfile(msg.outputPath.as_posix()):
			shutil.copy(msg.outputPath.as_posix(), msg.concatInput)
		return msg

class CreateConcatFileList(Filter):
	def Execute(self, msg):
		with open(msg.tempDir.name + '/files.txt', 'w') as file:
			if os.path.isfile(msg.concatInput):
				file.write("file '%s'\n"%msg.concatInput)
			file.write("file '%s'\n"%msg.tempOutput)
		msg.concatFileList = msg.tempDir.name + '/files.txt'
		return msg

class ConcatFile(Filter):
	def Execute(self, msg):
		msg.tempOutput = msg.tempDir.name + '/output.concat.' + config.FFMPEG_CONTAINER
		if not concatFile(msg.concatFileList, msg.tempOutput):
			raise StopProcessingError('Failed to concat files %s, %s'%(msg.origin_path, msg.outputPath))
		return msg

class ModeRouter(Router):
	def Excute(self, msg):
		if not msg.mode == lib.job.CONCAT:
			return msg
		else:
			return self.Route(msg)

	def Setup(self):
		self.Extend(DetermineConcatFile)
		self.Extend(CreateConcatFileList)
		self.Extend(CopyConcatFile)
		self.Extend(ConcatFile)
