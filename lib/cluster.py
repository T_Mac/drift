# This module should contain code to read/write to/from the etcd cluster
from lib.utils import CreateEtcdClient, dumpToEtcd, loadFromEtcd, findBaseName
from collections import OrderedDict
from lib.job import Status, Type
import pathlib


class Client:
	def __init__(self):
		self.etcd = CreateEtcdClient()

	def _write(self, key, data, append = False, prev = None):
		try:
			if prev:
				return self._compare(key, data, prev)
			else:
				resp = self.etcd.write(key, data, append = append)
		except:
			return False
		else:
			return resp.key

	def _compare(self, key, new, old):
		try:
			self.etcd.write(key, new, prevValue=old)
		except EtcdCompareFailed as e:
			return False
		else:
			return key

	def _read(self, key, recursive = False):
		try:
			resp = self.etcd.read(key, sorted = True, recursive=recursive)
		except:
			return None
		else:
			return resp

	def _iter(self, key, recursive = False):
		try:
			for result in self._etcd.read(key, sorted=True, recursive=recursive).children:
				if not result.dir:
					yield result
		except:
			pass



	def _findRelativeKeys(self, key, positions=[]):
		parentKey = key[:key.rfind('/')]
		# Create a list of just the keys found in the parent directory
		results = [x.key for x in self._read(parentKey).children]
		# using index() find the location for our key
		index = results.index(key)
		keys = []
		# For each passed position try to read it from the list and append the key
		# if it doesn't exist append None
		for position in positions:
			relativeIndex = index + position
			if not relativeIndex < 0 and not relativeIndex > len(results)-1:
				keys.append(results[relativeIndex])
			else:
				keys.append(None)
		return keys

	def _deriveBaseKey(self, msg): # Find the base path of msg.path
		return findBaseName(msg.path)

	def _createMessage(self, key, result):
		if not result.dir and result.value:
			data = loadFromEtcd(result.value)
			data['_prevKey'], data['_nextKey'] = self._findRelativeKeys(key, positions = [-1, 1])
			data['_etcd'] = self
			return EtcdMessage(key, **data)
		return None

	def get(self, key):
		result = self._read(key)
		return self._createMessage(key, result)


	def set(self, msg):
		if msg.key:
			return self._write(msg.key, msg.changed, prev = msg.current)
		else:
			key = self._deriveBaseKey(msg)
			msg.key = self._write(key, msg.changed, append = True)
			return msg

	def iter(self, key, recursive = False):
		for result in self._iter(key, recursive):
			yield self._createMessage(key, result)



class EtcdMessage:
	Structure = (
		'status',
		'mode',
		'path',
		'machine-id',
		'error'
	)
	def __init__(self, key = None, **kwargs):
		self._currentMessage = OrderedDict()
		self._changedMessage = dict()
		self._keyPath = key

		for entry in EtcdMessage.Structure:
			self._currentMessage[entry] = kwargs.pop(entry, None)

		for key, value in kwargs.items():
			setattr(self, key, value)

	def _mergeChanges(self):
		copy = self._currentMessage.copy()
		copy.update(self._changedMessage)
		return copy

	def _merge(self):
		self._currentMessage.update(self._changedMessage)
		self._changedMessage = {}

	def _commit(self):
		self._committed = False
		if self.key and getattr(self, '_etcd', None):
			success = self._etcd.set(self)
			if success:
				self._merge()
				self._committed = True

	@property
	def committed(self):
		return self._committed

	@property
	def current(self):
		return dumpToEtcd(self._currentMessage)

	@property
	def changed(self):
		return dumpToEtcd(self._mergeChanges())

	@property
	def mode(self):
		return self._mergeChanges().get('mode', None)

	@property
	def path(self):
		path = self._mergeChanges().get('path', None)
		if path:
			return pathlib.Path(path)
		return None

	@property
	def status(self):
		return self._mergeChanges().get('status', None)

	@status.setter
	def status(self, value):
		self._changedMessage['status'] = value
		self._commit()

	@property
	def error(self):
		return self._mergeChanges().get('error', None)

	@error.setter
	def error(self, value):
		self._changedMessage['error'] = value
		self._changedMessage['status'] = Status.ERROR
		self._commit()

	@property
	def machine_id(self):
		return self._mergeChanges().get('machine-id', None)

	@machine_id.setter
	def machine_id(self, value):
		self._changedMessage['machine-id'] = value
		self._commit()

	@property
	def key(self):
		return getattr(self, '_keyPath', None)

	@key.setter
	def key(self, value):
		self._keyPath = value

	@property
	def prevKey(self):
		return self._etcd.get(key)

	@property
	def nextKey(self):
		return self._etcd.get(key)
#
#
# class EtcdMessage():
# 	def __init__(self, keyPath):
# 		self._keyPath = keyPath
# 		self._currentMessage = self._loadFromEtcd()
# 		self._changedMessage = self._currentMessage.copy()
# 		self.logger = moduleLogger.getChild(keyPath)
#
# 	@property
# 	def status(self):
# 		return self._currentMessage.get('status', None)
#
# 	@property
# 	def path(self):
# 		return self._currentMessage.get('path', None)
#
# 	@property
# 	def mode(self):
# 		return self._currentMessage.get('mode', None)
#
# 	@property
# 	def key(self):
# 		return self._keyPath
#
# 	@property
# 	def prevKey(self):
# 		key = self.__dict__.get('_prevKey', None)
# 		if key:
# 			return EtcdMessage(key)
# 		else:
# 			return None
#
# 	@property
# 	def nextKey(self):
# 		key = self.__dict__.get('_nextKey', None)
# 		if key:
# 			return EtcdMessage(key)
# 		else:
# 			return None
#
# 	def _loadFromEtcd(self):
# 		raw = etcdClient.read(self._keyPath)
# 		if not raw.dir:
# 			self._isDir = False
# 			loaded = loadFromEtcd(raw.value)
# 			if loaded['mode'] == Type.CONCAT:
# 				self._prevKey, self._nextKey = lib.client.FindRelativeKeys(self._keyPath, positions=[-1,1])
# 			return loaded
# 		else:
# 			self._isDir = True
# 			return {'mode': Type.CONCAT}
#
# 	def _writeToEtcd(self):
# 		new = dumpToEtcd(self._changedMessage)
# 		self.logger.debug('Writing to etcd: %s'%new)
# 		old = dumpToEtcd(self._currentMessage)
# 		success = CompareAndSwap(self._keyPath, new, old)
# 		if success:
# 			self.logger.debug('Write Succeeded')
# 			self._currentMessage = self._changedMessage.copy()
# 			return True
# 		else:
# 			self.logger.error('Write Failed! new: %s - old: %s'%(new, old))
# 			return False
#
# 	def _updateStatus(self, status, error = None):
# 		self._changedMessage['status'] = status
# 		if error:
# 			self._changedMessage['error'] = error
# 		return self._writeToEtcd()
#
# 	def UpdateStatus(self, status, error = None):
# 		if self._isDir:
# 			return None
# 		return self._updateStatus(status, error)
#
# 	def SetWorkerId(self, workerId):
# 		self._changedMessage['worker'] = workerId
