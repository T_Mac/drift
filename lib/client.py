from lib.utils import CreateEtcdClient
etcdClient = CreateEtcdClient()

def FindRelativeKeys(key, positions = []):
	parentKey = key[:key.rfind('/')]
	# Create a list of just the keys found in the parent directory
	results = [x.key for x in etcdClient.read(parentKey, sorted=True).children]
	# using index() find the location for our key
	index = results.index(key)
	keys = []
	# For each passed position try to read it from the list and append the key
	# if it doesn't exist append None
	for position in positions:
		relativeIndex = index + position
		if not relativeIndex < 0 and not relativeIndex > len(results)-1:
			keys.append(results[relativeIndex])
		else:
			keys.append(None)
	return keys

def findPreviousKey(key):
	results = FindRelativeKeys(key, positions=[-1])
	if results[0]:
		return results[0]
	else:
		return None

def findNextKey(key):
	results = FindRelativeKeys(key, positions=[1])
	if results[0]:
		return results[0]
	else:
		return None
