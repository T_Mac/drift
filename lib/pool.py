from lib.queue import UniqueQueue
from lib.locker import LockSafeFitting
from lib.filters import *
from lib.job import CreateJob
from threading import Thread, Event, Semaphore
import time
import queue
import config
import pdb
moduleLogger = config.getLogger('pool')
class WorkerPool(object):
	def __init__(self, workerId):
		self.jobQueue = UniqueQueue()
		self._sem = Semaphore(value=1)
		self.pipeline = LockSafeFitting()
		self.__assemblePipeline()

		self.__processing = Event()

		self.workers = []
		self.workerId = workerId
		# for x in range(int(config.WORKERS)):
		# 	self.workers.append(Worker(self.jobQueue, self.pipeline, self.__processing))

	def startProcessing(self):
		# for worker in self.workers:
		# 	worker.start()
		self.__processing.set()

	def stopProcessing(self):
		self.__processing.clear()

	def isProcessing(self):
		return self.__processing.isSet()

	def addJob(self, msg):
		job = CreateJob(msg, self.workerId)
		moduleLogger.debug('Job created for %s'%job)
		self._sem.acquire()
		self._startWorker(job)
		return True

	def isFull(self):
		resp = self._sem.acquire(blocking=False)
		if resp:
			self._sem.release()
		return not resp

	def _startWorker(self, job):
		w = Worker(job, self.pipeline, self.__processing, self._sem)
		self.workers.append(w)
		w.start()
		return w

	def queueLength(self):
		return self.jobQueue.qsize()

	def join(self, timeout = 0):
		self.__processing.clear()
		for worker in self.workers:
			worker.join(timeout=0)

		return True

	def inQueue(self, job):
		job = CreateJob(job)
		return job in self.jobQueue

	def __assemblePipeline(self):
		self.pipeline.Register(TakeLock)
		self.pipeline.Register(CreateTempDirectory)
		self.pipeline.Register(CopyToDisk)
		self.pipeline.Register(InspectFile)
		self.pipeline.Register(SetupEncoding)
		self.pipeline.Register(EncodeVideo)
		self.pipeline.Register(CheckForOutput)
		self.pipeline.Register(DetermineDestination)
		self.pipeline.Register(ModeRouter)
		self.pipeline.Register(CreateDestDirectories)
		self.pipeline.Register(CopyToDestination)
		self.pipeline.Register(CleanupTempDir)
		self.pipeline.Register(RemoveOriginal)
		self.pipeline.Register(ReleaseLock)

class Worker(Thread):
	def __init__(self, job, pipeline, canProcess, sem):
		# self.queue = jobQueue
		self.job = job
		self.pipeline = pipeline
		self.canProcess = canProcess
		self._sem = sem
		super(Worker, self).__init__()
		self.daemon = True

	def run(self):
		moduleLogger.debug('Worker started for job %s'%self.job.origin_path)
		if self.canProcess.isSet():
			self.pipeline.convertFile(self.job)

		self._sem.release()
