from pipewrench.pipeline import Message
import pathlib
from lib.utils import loadFromEtcd, dumpToEtcd, CreateEtcdClient
import config
from lib.locker import CompareAndSwap
etcdClient = CreateEtcdClient()
import lib.client
moduleLogger = config.getLogger('job')
# Job States
QUEUED = 0
STARTED = 1
FINISHED = 2
ERROR = 3
# Job Modes
CONVERT = 0
CONCAT = 1

class Status():
	QUEUED = 0
	STARTED = 1
	FINISHED = 2
	ERROR = 3

class Type():
	CONVERT = 0
	CONCAT = 1

class Job(Message):
	def __init__(self, jobMsg, workerId, **kwargs):
		self.jobMsg = jobMsg
		self.jobMsg.SetWorkerId(workerId)
		self.origin_path = None
		self.locked = False
		super(Job, self).__init__(**kwargs)

	def __eq__(self, other):
		return other == self.origin_path

	def __hash__(self):
		return hash(self.origin_path)

	def StartJob(self):
		canStart = False
		if self.jobMsg.mode == Type.CONCAT:
			canStart == True
		else:
			prevKey = self.jobMsg.prevKey
			if prevKey:
				if prevKey.status == Status.FINISHED:
					canStart = True
			else:
				canStart = True

		if self.jobMsg.status == Status.QUEUED:
			success = self.jobMsg.UpdateStatus(Status.STARTED)
			if success:
				self.locked = True
				return True
		else:
			return False

	def FinishJob(self):
		return self.jobMsg.UpdateStatus(Status.FINISHED)

	def ReleaseJob(self):
		return self.jobMsg.UpdateStatus(Status.QUEUED)

	def MarkErrored(self, error):
		return self.jobMsg.UpdateStatus(Status.ERROR, error)


class EtcdMessage():
	def __init__(self, keyPath):
		self._keyPath = keyPath
		self._currentMessage = self._loadFromEtcd()
		self._changedMessage = self._currentMessage.copy()
		self.logger = moduleLogger.getChild(keyPath)

	@property
	def status(self):
		return self._currentMessage.get('status', None)

	@property
	def path(self):
		return self._currentMessage.get('path', None)

	@property
	def mode(self):
		return self._currentMessage.get('mode', None)

	@property
	def key(self):
		return self._keyPath

	@property
	def prevKey(self):
		key = self.__dict__.get('_prevKey', None)
		if key:
			return EtcdMessage(key)
		else:
			return None

	@property
	def nextKey(self):
		key = self.__dict__.get('_nextKey', None)
		if key:
			return EtcdMessage(key)
		else:
			return None

	def _loadFromEtcd(self):
		raw = etcdClient.read(self._keyPath)
		if not raw.dir:
			self._isDir = False
			loaded = loadFromEtcd(raw.value)
			if loaded['mode'] == Type.CONCAT:
				self._prevKey, self._nextKey = lib.client.FindRelativeKeys(self._keyPath, positions=[-1,1])
			return loaded
		else:
			self._isDir = True
			return {'mode': Type.CONCAT}

	def _writeToEtcd(self):
		new = dumpToEtcd(self._changedMessage)
		self.logger.debug('Writing to etcd: %s'%new)
		old = dumpToEtcd(self._currentMessage)
		success = CompareAndSwap(self._keyPath, new, old)
		if success:
			self.logger.debug('Write Succeeded')
			self._currentMessage = self._changedMessage.copy()
			return True
		else:
			self.logger.error('Write Failed! new: %s - old: %s'%(new, old))
			return False

	def _updateStatus(self, status, error = None):
		self._changedMessage['status'] = status
		if error:
			self._changedMessage['error'] = error
		return self._writeToEtcd()

	def UpdateStatus(self, status, error = None):
		if self._isDir:
			return None
		return self._updateStatus(status, error)

	def SetWorkerId(self, workerId):
		self._changedMessage['worker'] = workerId



def CreateJob(msg, workerId):
	path = pathlib.Path(msg.path)
	job = Job(msg, workerId, origin_path=path, mode = msg.mode)
	return job
