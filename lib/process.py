from threading import Thread, Event
import config
import select 
moduleLogger = config.getLogger('process')




class ProcTailer(Thread):
	def __init__(self, process, callBack = None):
		self.process = process
		self.callBack = callBack
		super(ProcTailer, self).__init__()
		self.daemon = True
		self.__exit = Event()

	def run(self):
		while not self.__exit.isSet():
			ready = select.select((self.process.stderr,),(),(), 5)
			if len(ready[0]) == 1:
				for entry in self.process.stderr:
					line = str(entry)
					#if 'Duration' in line:
					# output = self.process.stderr.read()
					moduleLogger.debug(line.strip())
					if self.callBack:
						self.callBack(line)

	def join(self, *args, **kwargs):
		self.__exit.set()
		return super(ProcTailer, self).join(*args, **kwargs)
