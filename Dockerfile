FROM gliderlabs/alpine:3.4

ENV FFMPEG_VERSION=3.0.2

RUN apk --update add python3 grep build-base curl nasm tar bzip2 \
				zlib-dev openssl-dev yasm-dev lame-dev libogg-dev speex-dev \
				x264-dev libvpx-dev libvorbis-dev x265-dev freetype-dev \
				libass-dev libwebp-dev rtmpdump-dev libtheora-dev opus-dev

ADD http://ffmpeg.org/releases/ffmpeg-${FFMPEG_VERSION}.tar.gz /tmp/ffmpeg.tar.gz
RUN cd /tmp && tar -xvzf ffmpeg.tar.gz

RUN cd /tmp/ffmpeg-${FFMPEG_VERSION} && \
	./configure \
  	--enable-version3 --enable-gpl --enable-nonfree --enable-small --enable-libmp3lame --enable-libx264 \
	--enable-libx265 --enable-libvpx --enable-libtheora --enable-libvorbis --enable-libopus --enable-libass \
	--enable-libwebp --enable-librtmp --enable-postproc --enable-avresample --enable-libfreetype \
	--enable-openssl --disable-debug --enable-libspeex && \
	make && \
	make install && \
	make distclean && \
	cd / && \
 	rm -rf /tmp

RUN apk del build-base curl tar bzip2 x264 openssl nasm && rm -rf /var/cache/apk/*

RUN mkdir /app /media/input /media/output
WORKDIR /app

ADD ./requirements.txt /app/
RUN pip3 install -r requirements.txt

ADD . /app

CMD python3 drift.py
